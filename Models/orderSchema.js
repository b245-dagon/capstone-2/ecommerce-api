const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    userId: {
        type:String,
        required:[true, "User id required!"]
    },
    products:[{
        productId:{
            type: String,
            required: [true, "Product id required!"]
        },
        quantity:{
            type: Number,
            default: 1
        }
    }],
    totalAmount:{
        type: Number,
        default: 0
    },
    purchaseOn:{
        type: Date,
        default: new Date()
    }, 
   


})





module.exports= mongoose.model("Order", orderSchema)












