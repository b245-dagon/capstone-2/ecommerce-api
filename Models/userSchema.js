const mongoose = require("mongoose");

const userSchema =  new mongoose.Schema({
    userName:{
        type: String,
        required:[false]
    },
    firstName:{
        type: String,
        required:[false]
    },
    lastName:{
        type: String,
        required:[false]
    },

    email:{
        type: String,
        required:[true, "Please input email"]

        
    },
    password:{
        type: String,
        required:[true,"Please input Password"]
    },
    phoneNumber:{
        type: Number,
        required: [false]
    },
    isAdmin:{
        type: Boolean,
        default: false
    }
})

module.exports = mongoose.model("User",userSchema);