const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
    userId: {
        type:String,
        required:[true, "User id required!"]
    },
    products:[{
        productId:{
            type: String,
            required: [true, "Product id required!"]
        },
        quantity:{
            type: Number,
            default: 0
        },
        priceAmount: {
            type: Number,
            default: 0
        }
        
    }],
    subtotalAmount:{
        type: Number,
        default: 0
    }
   


})





module.exports= mongoose.model("Cart", cartSchema)