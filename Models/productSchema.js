const mongoose = require("mongoose");

const productSchema =  new mongoose.Schema({
    productName:{
        type: String,
        required:[true,"Product Name required!"]
    },

    description: {
        type: String,
        required:[true, "Product description required!"]
    },
    stocks: {
        type: Number,
        default: 0,
        required:[true,"Product stock required!"]
    },

    price:{
        type: Number,
        required:[true, "Product price required!"]
    },
    createdOn:{
        type: Date,
        default: new Date()

    },
    isActive:{
        type: Boolean,
        default:true
    },
    productImage:{
        type: String,

    }
    

    
})

module.exports = mongoose.model("Products",productSchema);