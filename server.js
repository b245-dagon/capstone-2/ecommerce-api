const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors")

// Creating port
const port = 3001;


const app =express();

const userRoutes = require("./Routes/userRoutes.js")
const productRoutes = require("./Routes/productRoutes.js")
//middle wares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

//routing section

app.use("/user",userRoutes);
app.use("/product",productRoutes);


//mongoose section
mongoose.set("strictQuery", true);
mongoose.connect("mongodb+srv://admin:admin@batch245-dagon.wirp2aj.mongodb.net/batch245_Ecommerce_API_Dagon?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console,"Connection Error!"));
db.once("open",()=>{
    console.log("We are connected to the cloud")
});

//Checking Server console if is running
app.listen(port,()=>{
    console.log(`Server is running at port ${port}`)
})