const express = require("express")

const router = express.Router();

const productController = require("../Controllers/productController.js");
const orderController = require("../Controllers/orderController.js")

const cartController = require("../Controllers/cartController.js")



const auth = require("../auth.js")

//Routes

router.post("/register",auth.verify, productController.addProduct);

router.get("/",productController.getProducts);

router.get("/allOrders",auth.verify,orderController.allOrders)

router.get("/allProducts",auth.verify,productController.getAllProducts)

router.get("/myOrder",auth.verify,cartController.viewCart)
// routes with params id

router.post("/createOrder/:productId",auth.verify, orderController.createOrder);

router.get("/:productId", auth.verify,productController.getSingleProduct);

router.put("/update/:productId",auth.verify,productController.updateProduct);

router.put("/archiveProduct/:productId",auth.verify,productController.archiveProduct)

//ADD TO CART SECTION

router.post("/addCart/:productId",auth.verify,cartController.addCart)
// router.post("/addToCart/:cartId",auth.verify,cartController.addToCart)


module.exports = router;
