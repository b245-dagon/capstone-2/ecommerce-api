const express = require("express")

const router = express.Router();

const userController = require("../Controllers/userController.js");
const GetAllUser = require("../Controllers/userController")
const cartController = require("../Controllers/cartController.js")

const auth = require("../auth.js")

//Routes for registration
router.post("/register",userController.userRegistration);
//routes for profile
// router.post("/profile",userController.userProfile);

router.get("/allUser",auth.verify,userController.GetAllUser)

router.get("/myOrder",auth.verify,cartController.viewCart)



// Route for  userauthentication
router.post("/login", userController.userAuthentication);
router.post("/admin",userController.adminAuthentication);

// routes for params
router.get("/details",userController.UserDetails);
router.put("/profile/:id", userController.updateProfile);



module.exports = router;