const mongoose = require("mongoose")

const Product =require("../Models/productSchema.js");

const Order = require ("../Models/orderSchema.js")

const User = require("../Models/userSchema.js")





const auth =require("../auth.js");


module.exports.addProduct = (request, response)=>{

    const adminData =auth.decode(request.headers.authorization)
   
    let input= request.body
    
        if(adminData.isAdmin == true){
            let newProduct = new Product({
                productName: input.productName,
                description: input.description,
                stocks: input.stocks,
                price: input.price,
                image: input.image
                
                })
            
        return newProduct.save()
        // couse succesfully created
        .then(result =>{
            response.send(true);
        }).catch(error=>{
        
            response.send(false)
        })
            
        }else{ 
            return response.send(false)
    
        }
    }

module.exports.getAllProducts = (request,response) =>{

    const adminData =auth.decode(request.headers.authorization)

    if(!adminData.isAdmin){
        return response.send(false)
    }else{
        Product.find({}).then(result =>{
            response.send(result)
        }).catch(error => response.send(false))
    }
    }





    // Retrieving all active products



module.exports.getProducts = (request,response)=>{

    let idIsActive = request.body.isActive
    
    Product.aggregate([{$match: {isActive: true}}])
    .then(result => {
        return response.send(result)
        
       
    }).catch(error => {
        return response.send(error)
    });



}
module.exports.getSingleProduct = (request,response)=>{
    const adminData =auth.decode(request.headers.authorization)
    
    let idToBeFind = request.params.productId;

    if(!adminData.isAdmin){
    Product.findById(idToBeFind)
    .then(result =>{
     
            return response.send(result)

         })
      
      
    .catch(error => {
                return response.send(error)
            })

    }else{
        return response.send("You are not authorized")
    }
        
}

// Product Updating
module.exports.updateProduct= (request, response)=>{
    const adminData =auth.decode(request.headers.authorization)
    const input = request.body;
    
    let productToBeUpdated = request.params.productId;
    if(adminData.isAdmin){
        Product.findByIdAndUpdate(productToBeUpdated,{
            productName: input.productName,
            description: input.description,
            stocks: input.stocks,
            price: input.price,
            category: input.category,
            productImage: input.productImage
            
    
        },{new:true})
        .then(result =>{
                
                return response.send(true)
    
             })
          
          
        .catch(error => {
                    return response.send(false)
                })

    }else{
        return response.send(false)
    }
    
        
}
   
module.exports.archiveProduct = (request, response) =>{
    const adminData =auth.decode(request.headers.authorization)
    
    let productToBeArchive = request.params.productId;
     const input = request.body
    if(!adminData.isAdmin){
        return response.send(false);
    }else{

        Product.findById({_id:productToBeArchive})
        .then(result =>{
            if(result === null){
                return response.send(false);
            }else{
                if(result.isActive){
                    
                    let archive = {isActive:false}

                    Product.findByIdAndUpdate(productToBeArchive, archive,{new:true})
                    .then(result => response.send(true))
                    .catch(error =>{
                        return response.send(false)
                    });

                }else{
                    let goactive ={isActive: true}
                    
                    Product.findByIdAndUpdate(productToBeArchive, goactive,{new:true})
                    .then(result => response.send(true))
                    .catch(error =>{
                        return response.send(false)
                    });


                }
            }
        })
        .catch(error =>{
            return response.send(false)
        })
    }
}