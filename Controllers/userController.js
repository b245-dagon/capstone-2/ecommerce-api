const mongoose = require("mongoose");

const User = require("../Models/userSchema.js");

const bcrypt = require("bcrypt");
 
const auth =require("../auth.js")

// Controllers

// This controller will create or register a user on our database

module.exports.userRegistration = (request, response)=>{ 
    const input = request.body;
    User.findOne({email: input.email})
    .then(result =>{
        if(result !== null){
            return response.send(false)
        }else{
            let newUser = new User({
                userName:input.userName,
                firstName: input.firstName,
                lastName: input.lastName,
                email: input.email,
                password: bcrypt.hashSync(input.password, 10),
                phoneNumber: input.phoneNumber
            })
            //save to database
            newUser.save()
            .then(save =>{
                return response.send(true)

            }).catch(error =>{
                return response.send(false)
            })
        }
    }).catch(error=>{
        return response.send(false)
    })
}

// User Authentication Section
module.exports.userAuthentication = (request, response)=>{

    let input = request.body;
    User.findOne({email: input.email})
    .then(result =>{
        if(result === null){
            return response.send(false)
        }else{
            const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)
        if(isPasswordCorrect){
            return response.send({auth: auth.createAccessToken(result)})

        }else{
            return response.send(false)
        }
       
        }


    }).catch(error =>{
        return response.send(false)
    })
}


module.exports.updateProfile= (request, response)=>{
    const input = request.body;
    
    let idToBeUpdated = request.params.id;

    User.findByIdAndUpdate(idToBeUpdated,{
        password: "",
        firstName: input.firstName,
        lastName: input.lastName,
        phoneNumber: input.phoneNumber

    },{new:true})
    .then(result =>{
     
            return response.send(result)

         })
      
      
    .catch(error => {
                return response.send(error)
            })
        
}
   
// Admin login
module.exports.adminAuthentication = (request, response)=>{

    let input = request.body;
    User.findOne({userName: input.userName})
    .then(result =>{
        if(result === null){
            return response.send("Username is incorrect")
        }else{
            const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)
        if(isPasswordCorrect){
            return response.send({auth: auth.createAccessToken(result)})

        }else{
            return response.send("Password is incorrect!")
        }
       
        }


    }).catch(error =>{
        return response.send(error)
    })
}

//Getting user details
module.exports.UserDetails = (request,response)=>{
    
    
    const userData =auth.decode(request.headers.authorization)
 

    User.findById(userData)
    .then(result =>{
     
            return response.send(result)

         })
      
      
    .catch(error => {
                return response.send(error)
            })
        
}

module.exports.GetAllUser = (request,response)=>{
    const userData =auth.decode(request.headers.authorization)
    
    if(!userData.isAdmin){
        return response.send(false)
    }else{
        User.find({})
        .then(result =>{
            return response.send(result)
        })
        .catch(error => response.send(false))
    }
    
    
}
