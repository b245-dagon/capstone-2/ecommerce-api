const mongoose = require("mongoose");


const Product =require("../Models/productSchema.js");
const auth =require("../auth.js")
const Order = require("../Models/orderSchema.js")
const User = require("../Models/userSchema")




module.exports.createOrder = async (request, response)=>{
    const userData = auth.decode(request.headers.authorization)
    
    let input= request.body

    let productToBeGet= request.params.productId

    

    if(userData.isAdmin=== true){
        return response.send(false)
    }else{
        await User.findById(userData._id)
        .then(result =>{
            console.log(result)
            if(result === null ){
                console.log(result)
                return response.send(false)
            }else{
                Product.findById(productToBeGet)
                .then(result =>{
                    console.log(result)
                    if(result===null){
                        return response.send(false)
                    }else{
                        let order = new Order({
                            userId:userData._id,
                            products :[{    
                                    productId: result._id,
                                    quantity:  input.quantity,
                                   
                                            }],
                            totalAmount: result.price * input.quantity
            
            
                                                     })
                         return order.save()
                         .then(result =>{
                            return response.send(order)
                         }).catch(error =>{
                            return response.send(false)
                         })

                    }
                })
               
            }
        })
    }
}




module.exports.allOrders = (request, response) =>{

    const userData = auth.decode(request.headers.authorization)

    if(userData.isAdmin){

        Order.find({})
        .then(result =>{
            return response.send(result)
        }).catch(error =>{
            return response.send(false)
        })
    }else{
        return response.send(false)
    }


}

module.exports.myOrder = (request, response) =>{

    const userData = auth.decode(request.headers.authorization)

    if(userData.isAdamin){
        return response.send(false)
    }else{
        Order.findById(userData._id)
    }


}