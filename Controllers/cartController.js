const mongoose = require("mongoose");


const Product =require("../Models/productSchema.js");
const auth =require("../auth.js")
const Cart = require("../Models/cartSchema.js")
const User = require("../Models/userSchema.js")
const Order =require("../Models/orderSchema")



//AddCart SECTION
module.exports.addCart = async (request, response)=>{
    const userData = auth.decode(request.headers.authorization)
    
    let input= request.body

    let productToBeGet= request.params.productId

    

    if(userData.isAdmin=== true){
        return response.send(false)
    }else{
        await User.findById(userData._id)
        .then(result =>{
            if(result === null ){
                return response.send(false)
            }else{
                Product.findById(productToBeGet)
                .then(result =>{
                    if(result===null){
                        return response.send(false)
                    }else{
                        let newCart = new Cart({
                            userId:userData._id,
                            products :[{    
                                    productId: input._id,
                                    quantity:  input.quantity,
                                    priceAmount: result.price * input.quantity
                                            }],
            
            
                                                     })
                         newCart.save()
                         .then(result =>{
                            return response.send(true)
                         }).catch(error =>{
                            return response.send(false)
                         })

                    }
                })
               
            }
        })
    }

}

    
         

module.exports.viewCart = (request, response) =>{
    const userData = auth.decode(request.headers.authorization)

    if(!userData.isAdmin){
    Order.find({})
    .then(result => response.send(result))
    .catch(error => {
        return response.send(false)
    })
    }else{
        return false
    }
}

// module.exports.addToCart = async (request, response) => {
//   const userData = auth.decode(request.headers.authorization);
//   let user = userData._id
//   let input = request.body
//   const cartId = request.params.cartId;
//   if(userData.isAdmin){
//           return response.send("Something went wrong")

//   }else{
//     await Cart.findById(user)
//     .then(result =>{
//       Cart.findById(cartId)
//       .then(result =>{
//         result.products.push({productId:input.productId, quantity:input.quantity})
//             result.save()
//             let sum = 0;
//              console.log(result.products[0].productId)
//              Object.keys(result.products).forEach(key => sum += result.products[key].quantity || 0)
//              console.log(sum)
//            })
//        })

//     }
// }

        
  