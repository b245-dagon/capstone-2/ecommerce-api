const jwt = require ("jsonwebtoken")



const secret = "EcommerceAPI";

//Token creation




module.exports.createAccessToken = (user) =>{
    const data = {
        //payload
        _id: user.id,
        email: user.email,
        isAdmin: user.isAdmin

    }

    return jwt.sign(data, secret,{});

}

//token verifivation

module.exports.verify = (request, response, next)=>{
   
    // Token is retrieved from the request headers
  
        let token = request.headers.authorization;

       

        // Token recieved and is not undefined

        if(typeof token !== "undefined"){   
            
           
            token=token.slice(7, token.length);

           
            return jwt.verify(token,secret,(err,data)=>{
                if(err){
                    return response.send({auth:"Failed."})
                }else{
                    next();
                }
            })
        }else{
            return response.send({auth: "Failed"})
        }

}

// Decoded Section

module.exports.decode = (token) => {
    //token receive is not undefined 
    if(typeof token !== "undefined"){
        token = token.slice(7,token.length);

        return jwt.verify(token, secret, (err, data)=>{
            if(err){
                return null;
            }else{
                
                return jwt.decode(token,{complete:true}).payload;
            }
        })

    }else{
        return null;
    }
}